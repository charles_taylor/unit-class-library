using System;

 namespace UnitClassLibrary
{
	/// <summary> Enum for specifying the type of unit a Energy is.</summary>
	public enum EnergyType { Calorie, Kilocalorie, Erg, Footpound, Joule }
}