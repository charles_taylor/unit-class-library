using System;

 namespace UnitClassLibrary
{
	/// <summary> Enum for specifying the type of unit a Capacitance is.</summary>
	public enum CapacitanceType { Picofarad, Nanofarad, Microfarad, Millifarad, Farad, Abfarad, Statfarad }
}