using System;

 namespace UnitClassLibrary
{
	/// <summary> Enum for specifying the type of unit a ElectricCurrent is.</summary>
	public enum ElectricCurrentType { Ampere, Milliampere, Microampere }
}