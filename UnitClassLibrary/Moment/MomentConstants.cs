using System;

 namespace UnitClassLibrary
{

	public partial class Moment
	{

		public static Moment NewtonsMillimeter
		{
			get { return new Moment(MomentType.NewtonsMillimeter, 1); }
		}

		public static Moment NewtonsCentimeter
		{
			get { return new Moment(MomentType.NewtonsCentimeter, 1); }
		}

		public static Moment NewtonsMeter
		{
			get { return new Moment(MomentType.NewtonsMeter, 1); }
		}

		public static Moment NewtonsKilometer
		{
			get { return new Moment(MomentType.NewtonsKilometer, 1); }
		}

		public static Moment NewtonsInch
		{
			get { return new Moment(MomentType.NewtonsInch, 1); }
		}

		public static Moment NewtonsFoot
		{
			get { return new Moment(MomentType.NewtonsFoot, 1); }
		}

		public static Moment NewtonsYard
		{
			get { return new Moment(MomentType.NewtonsYard, 1); }
		}

		public static Moment NewtonsMile
		{
			get { return new Moment(MomentType.NewtonsMile, 1); }
		}

		public static Moment PoundsMillimeter
		{
			get { return new Moment(MomentType.PoundsMillimeter, 1); }
		}

		public static Moment PoundsCentimeter
		{
			get { return new Moment(MomentType.PoundsCentimeter, 1); }
		}

		public static Moment PoundsMeter
		{
			get { return new Moment(MomentType.PoundsMeter, 1); }
		}

		public static Moment PoundsKilometer
		{
			get { return new Moment(MomentType.PoundsKilometer, 1); }
		}

		public static Moment PoundsInch
		{
			get { return new Moment(MomentType.PoundsInch, 1); }
		}

		public static Moment PoundsFoot
		{
			get { return new Moment(MomentType.PoundsFoot, 1); }
		}

		public static Moment PoundsYard
		{
			get { return new Moment(MomentType.PoundsYard, 1); }
		}

		public static Moment PoundsMile
		{
			get { return new Moment(MomentType.PoundsMile, 1); }
		}

		public static Moment KipsMillimeter
		{
			get { return new Moment(MomentType.KipsMillimeter, 1); }
		}

		public static Moment KipsCentimeter
		{
			get { return new Moment(MomentType.KipsCentimeter, 1); }
		}

		public static Moment KipsMeter
		{
			get { return new Moment(MomentType.KipsMeter, 1); }
		}

		public static Moment KipsKilometer
		{
			get { return new Moment(MomentType.KipsKilometer, 1); }
		}

		public static Moment KipsInch
		{
			get { return new Moment(MomentType.KipsInch, 1); }
		}

		public static Moment KipsFoot
		{
			get { return new Moment(MomentType.KipsFoot, 1); }
		}

		public static Moment KipsYard
		{
			get { return new Moment(MomentType.KipsYard, 1); }
		}

		public static Moment KipsMile
		{
			get { return new Moment(MomentType.KipsMile, 1); }
		}
	}
}