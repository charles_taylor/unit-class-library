using System;

 namespace UnitClassLibrary
{
	/// <summary> Enum for specifying the type of unit a Moment is.</summary>
	public enum MomentType { NewtonsMillimeter, NewtonsCentimeter, NewtonsMeter, NewtonsKilometer, NewtonsInch, NewtonsFoot, NewtonsYard, NewtonsMile, PoundsMillimeter, PoundsCentimeter, PoundsMeter, PoundsKilometer, PoundsInch, PoundsFoot, PoundsYard, PoundsMile, KipsMillimeter, KipsCentimeter, KipsMeter, KipsKilometer, KipsInch, KipsFoot, KipsYard, KipsMile }
}