using System;

 namespace UnitClassLibrary
{
	/// <summary> Enum for specifying the type of unit a Time is.</summary>
	public enum TimeType { Nanosecond, Microsecond, Millisecond, Second, Minute, Hour, Day, Week, Month, Year, Decade, Century }
}