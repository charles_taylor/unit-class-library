using System;

 namespace UnitClassLibrary
{
	/// <summary> Enum for specifying the type of unit a Force is.</summary>
	public enum ForceType { Newton, Pound, Kip }
}