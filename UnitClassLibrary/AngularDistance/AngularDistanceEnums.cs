using System;

 namespace UnitClassLibrary
{
	/// <summary> Enum for specifying the type of unit a AngularDistance is.</summary>
	public enum AngleType
    { Radian, Degree }
}