using System;

 namespace UnitClassLibrary
{
	/// <summary> Enum for specifying the type of unit a Temperature is.</summary>
	public enum TemperatureType { Celsius, Fahrenheit, Kelvin, Rankine, Delisle, Reaumur, Romer }
}