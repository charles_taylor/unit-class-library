using System;

 namespace UnitClassLibrary
{

	public partial class Speed
	{
		public double MillimetersPerNanoseconds
		{
			get { return _distance.Millimeters / _time.Nanoseconds; }
		}
		public double MillimetersPerMicroseconds
		{
			get { return _distance.Millimeters / _time.Microseconds; }
		}
		public double MillimetersPerMilliseconds
		{
			get { return _distance.Millimeters / _time.Milliseconds; }
		}
		public double MillimetersPerSeconds
		{
			get { return _distance.Millimeters / _time.Seconds; }
		}
		public double MillimetersPerMinutes
		{
			get { return _distance.Millimeters / _time.Minutes; }
		}
		public double MillimetersPerHours
		{
			get { return _distance.Millimeters / _time.Hours; }
		}
		public double MillimetersPerDays
		{
			get { return _distance.Millimeters / _time.Days; }
		}
		public double MillimetersPerWeeks
		{
			get { return _distance.Millimeters / _time.Weeks; }
		}
		public double MillimetersPerMonths
		{
			get { return _distance.Millimeters / _time.Months; }
		}
		public double MillimetersPerYears
		{
			get { return _distance.Millimeters / _time.Years; }
		}
		public double MillimetersPerDecades
		{
			get { return _distance.Millimeters / _time.Decades; }
		}
		public double MillimetersPerCenturies
		{
			get { return _distance.Millimeters / _time.Centuries; }
		}
		public double CentimetersPerNanoseconds
		{
			get { return _distance.Centimeters / _time.Nanoseconds; }
		}
		public double CentimetersPerMicroseconds
		{
			get { return _distance.Centimeters / _time.Microseconds; }
		}
		public double CentimetersPerMilliseconds
		{
			get { return _distance.Centimeters / _time.Milliseconds; }
		}
		public double CentimetersPerSeconds
		{
			get { return _distance.Centimeters / _time.Seconds; }
		}
		public double CentimetersPerMinutes
		{
			get { return _distance.Centimeters / _time.Minutes; }
		}
		public double CentimetersPerHours
		{
			get { return _distance.Centimeters / _time.Hours; }
		}
		public double CentimetersPerDays
		{
			get { return _distance.Centimeters / _time.Days; }
		}
		public double CentimetersPerWeeks
		{
			get { return _distance.Centimeters / _time.Weeks; }
		}
		public double CentimetersPerMonths
		{
			get { return _distance.Centimeters / _time.Months; }
		}
		public double CentimetersPerYears
		{
			get { return _distance.Centimeters / _time.Years; }
		}
		public double CentimetersPerDecades
		{
			get { return _distance.Centimeters / _time.Decades; }
		}
		public double CentimetersPerCenturies
		{
			get { return _distance.Centimeters / _time.Centuries; }
		}
		public double MetersPerNanoseconds
		{
			get { return _distance.Meters / _time.Nanoseconds; }
		}
		public double MetersPerMicroseconds
		{
			get { return _distance.Meters / _time.Microseconds; }
		}
		public double MetersPerMilliseconds
		{
			get { return _distance.Meters / _time.Milliseconds; }
		}
		public double MetersPerSeconds
		{
			get { return _distance.Meters / _time.Seconds; }
		}
		public double MetersPerMinutes
		{
			get { return _distance.Meters / _time.Minutes; }
		}
		public double MetersPerHours
		{
			get { return _distance.Meters / _time.Hours; }
		}
		public double MetersPerDays
		{
			get { return _distance.Meters / _time.Days; }
		}
		public double MetersPerWeeks
		{
			get { return _distance.Meters / _time.Weeks; }
		}
		public double MetersPerMonths
		{
			get { return _distance.Meters / _time.Months; }
		}
		public double MetersPerYears
		{
			get { return _distance.Meters / _time.Years; }
		}
		public double MetersPerDecades
		{
			get { return _distance.Meters / _time.Decades; }
		}
		public double MetersPerCenturies
		{
			get { return _distance.Meters / _time.Centuries; }
		}
		public double KilometersPerNanoseconds
		{
			get { return _distance.Kilometers / _time.Nanoseconds; }
		}
		public double KilometersPerMicroseconds
		{
			get { return _distance.Kilometers / _time.Microseconds; }
		}
		public double KilometersPerMilliseconds
		{
			get { return _distance.Kilometers / _time.Milliseconds; }
		}
		public double KilometersPerSeconds
		{
			get { return _distance.Kilometers / _time.Seconds; }
		}
		public double KilometersPerMinutes
		{
			get { return _distance.Kilometers / _time.Minutes; }
		}
		public double KilometersPerHours
		{
			get { return _distance.Kilometers / _time.Hours; }
		}
		public double KilometersPerDays
		{
			get { return _distance.Kilometers / _time.Days; }
		}
		public double KilometersPerWeeks
		{
			get { return _distance.Kilometers / _time.Weeks; }
		}
		public double KilometersPerMonths
		{
			get { return _distance.Kilometers / _time.Months; }
		}
		public double KilometersPerYears
		{
			get { return _distance.Kilometers / _time.Years; }
		}
		public double KilometersPerDecades
		{
			get { return _distance.Kilometers / _time.Decades; }
		}
		public double KilometersPerCenturies
		{
			get { return _distance.Kilometers / _time.Centuries; }
		}
		public double InchesPerNanoseconds
		{
			get { return _distance.Inches / _time.Nanoseconds; }
		}
		public double InchesPerMicroseconds
		{
			get { return _distance.Inches / _time.Microseconds; }
		}
		public double InchesPerMilliseconds
		{
			get { return _distance.Inches / _time.Milliseconds; }
		}
		public double InchesPerSeconds
		{
			get { return _distance.Inches / _time.Seconds; }
		}
		public double InchesPerMinutes
		{
			get { return _distance.Inches / _time.Minutes; }
		}
		public double InchesPerHours
		{
			get { return _distance.Inches / _time.Hours; }
		}
		public double InchesPerDays
		{
			get { return _distance.Inches / _time.Days; }
		}
		public double InchesPerWeeks
		{
			get { return _distance.Inches / _time.Weeks; }
		}
		public double InchesPerMonths
		{
			get { return _distance.Inches / _time.Months; }
		}
		public double InchesPerYears
		{
			get { return _distance.Inches / _time.Years; }
		}
		public double InchesPerDecades
		{
			get { return _distance.Inches / _time.Decades; }
		}
		public double InchesPerCenturies
		{
			get { return _distance.Inches / _time.Centuries; }
		}
		public double FeetPerNanoseconds
		{
			get { return _distance.Feet / _time.Nanoseconds; }
		}
		public double FeetPerMicroseconds
		{
			get { return _distance.Feet / _time.Microseconds; }
		}
		public double FeetPerMilliseconds
		{
			get { return _distance.Feet / _time.Milliseconds; }
		}
		public double FeetPerSeconds
		{
			get { return _distance.Feet / _time.Seconds; }
		}
		public double FeetPerMinutes
		{
			get { return _distance.Feet / _time.Minutes; }
		}
		public double FeetPerHours
		{
			get { return _distance.Feet / _time.Hours; }
		}
		public double FeetPerDays
		{
			get { return _distance.Feet / _time.Days; }
		}
		public double FeetPerWeeks
		{
			get { return _distance.Feet / _time.Weeks; }
		}
		public double FeetPerMonths
		{
			get { return _distance.Feet / _time.Months; }
		}
		public double FeetPerYears
		{
			get { return _distance.Feet / _time.Years; }
		}
		public double FeetPerDecades
		{
			get { return _distance.Feet / _time.Decades; }
		}
		public double FeetPerCenturies
		{
			get { return _distance.Feet / _time.Centuries; }
		}
		public double YardsPerNanoseconds
		{
			get { return _distance.Yards / _time.Nanoseconds; }
		}
		public double YardsPerMicroseconds
		{
			get { return _distance.Yards / _time.Microseconds; }
		}
		public double YardsPerMilliseconds
		{
			get { return _distance.Yards / _time.Milliseconds; }
		}
		public double YardsPerSeconds
		{
			get { return _distance.Yards / _time.Seconds; }
		}
		public double YardsPerMinutes
		{
			get { return _distance.Yards / _time.Minutes; }
		}
		public double YardsPerHours
		{
			get { return _distance.Yards / _time.Hours; }
		}
		public double YardsPerDays
		{
			get { return _distance.Yards / _time.Days; }
		}
		public double YardsPerWeeks
		{
			get { return _distance.Yards / _time.Weeks; }
		}
		public double YardsPerMonths
		{
			get { return _distance.Yards / _time.Months; }
		}
		public double YardsPerYears
		{
			get { return _distance.Yards / _time.Years; }
		}
		public double YardsPerDecades
		{
			get { return _distance.Yards / _time.Decades; }
		}
		public double YardsPerCenturies
		{
			get { return _distance.Yards / _time.Centuries; }
		}
		public double MilesPerNanoseconds
		{
			get { return _distance.Miles / _time.Nanoseconds; }
		}
		public double MilesPerMicroseconds
		{
			get { return _distance.Miles / _time.Microseconds; }
		}
		public double MilesPerMilliseconds
		{
			get { return _distance.Miles / _time.Milliseconds; }
		}
		public double MilesPerSeconds
		{
			get { return _distance.Miles / _time.Seconds; }
		}
		public double MilesPerMinutes
		{
			get { return _distance.Miles / _time.Minutes; }
		}
		public double MilesPerHours
		{
			get { return _distance.Miles / _time.Hours; }
		}
		public double MilesPerDays
		{
			get { return _distance.Miles / _time.Days; }
		}
		public double MilesPerWeeks
		{
			get { return _distance.Miles / _time.Weeks; }
		}
		public double MilesPerMonths
		{
			get { return _distance.Miles / _time.Months; }
		}
		public double MilesPerYears
		{
			get { return _distance.Miles / _time.Years; }
		}
		public double MilesPerDecades
		{
			get { return _distance.Miles / _time.Decades; }
		}
		public double MilesPerCenturies
		{
			get { return _distance.Miles / _time.Centuries; }
		}

		public double GetValue(SpeedType Units)
		{
			switch (Units)
			{
				case SpeedType.MillimetersPerNanosecond:
					return MillimetersPerNanoseconds;
				case SpeedType.MillimetersPerMicrosecond:
					return MillimetersPerMicroseconds;
				case SpeedType.MillimetersPerMillisecond:
					return MillimetersPerMilliseconds;
				case SpeedType.MillimetersPerSecond:
					return MillimetersPerSeconds;
				case SpeedType.MillimetersPerMinute:
					return MillimetersPerMinutes;
				case SpeedType.MillimetersPerHour:
					return MillimetersPerHours;
				case SpeedType.MillimetersPerDay:
					return MillimetersPerDays;
				case SpeedType.MillimetersPerWeek:
					return MillimetersPerWeeks;
				case SpeedType.MillimetersPerMonth:
					return MillimetersPerMonths;
				case SpeedType.MillimetersPerYear:
					return MillimetersPerYears;
				case SpeedType.MillimetersPerDecade:
					return MillimetersPerDecades;
				case SpeedType.MillimetersPerCentury:
					return MillimetersPerCenturies;
				case SpeedType.CentimetersPerNanosecond:
					return CentimetersPerNanoseconds;
				case SpeedType.CentimetersPerMicrosecond:
					return CentimetersPerMicroseconds;
				case SpeedType.CentimetersPerMillisecond:
					return CentimetersPerMilliseconds;
				case SpeedType.CentimetersPerSecond:
					return CentimetersPerSeconds;
				case SpeedType.CentimetersPerMinute:
					return CentimetersPerMinutes;
				case SpeedType.CentimetersPerHour:
					return CentimetersPerHours;
				case SpeedType.CentimetersPerDay:
					return CentimetersPerDays;
				case SpeedType.CentimetersPerWeek:
					return CentimetersPerWeeks;
				case SpeedType.CentimetersPerMonth:
					return CentimetersPerMonths;
				case SpeedType.CentimetersPerYear:
					return CentimetersPerYears;
				case SpeedType.CentimetersPerDecade:
					return CentimetersPerDecades;
				case SpeedType.CentimetersPerCentury:
					return CentimetersPerCenturies;
				case SpeedType.MetersPerNanosecond:
					return MetersPerNanoseconds;
				case SpeedType.MetersPerMicrosecond:
					return MetersPerMicroseconds;
				case SpeedType.MetersPerMillisecond:
					return MetersPerMilliseconds;
				case SpeedType.MetersPerSecond:
					return MetersPerSeconds;
				case SpeedType.MetersPerMinute:
					return MetersPerMinutes;
				case SpeedType.MetersPerHour:
					return MetersPerHours;
				case SpeedType.MetersPerDay:
					return MetersPerDays;
				case SpeedType.MetersPerWeek:
					return MetersPerWeeks;
				case SpeedType.MetersPerMonth:
					return MetersPerMonths;
				case SpeedType.MetersPerYear:
					return MetersPerYears;
				case SpeedType.MetersPerDecade:
					return MetersPerDecades;
				case SpeedType.MetersPerCentury:
					return MetersPerCenturies;
				case SpeedType.KilometersPerNanosecond:
					return KilometersPerNanoseconds;
				case SpeedType.KilometersPerMicrosecond:
					return KilometersPerMicroseconds;
				case SpeedType.KilometersPerMillisecond:
					return KilometersPerMilliseconds;
				case SpeedType.KilometersPerSecond:
					return KilometersPerSeconds;
				case SpeedType.KilometersPerMinute:
					return KilometersPerMinutes;
				case SpeedType.KilometersPerHour:
					return KilometersPerHours;
				case SpeedType.KilometersPerDay:
					return KilometersPerDays;
				case SpeedType.KilometersPerWeek:
					return KilometersPerWeeks;
				case SpeedType.KilometersPerMonth:
					return KilometersPerMonths;
				case SpeedType.KilometersPerYear:
					return KilometersPerYears;
				case SpeedType.KilometersPerDecade:
					return KilometersPerDecades;
				case SpeedType.KilometersPerCentury:
					return KilometersPerCenturies;
				case SpeedType.InchesPerNanosecond:
					return InchesPerNanoseconds;
				case SpeedType.InchesPerMicrosecond:
					return InchesPerMicroseconds;
				case SpeedType.InchesPerMillisecond:
					return InchesPerMilliseconds;
				case SpeedType.InchesPerSecond:
					return InchesPerSeconds;
				case SpeedType.InchesPerMinute:
					return InchesPerMinutes;
				case SpeedType.InchesPerHour:
					return InchesPerHours;
				case SpeedType.InchesPerDay:
					return InchesPerDays;
				case SpeedType.InchesPerWeek:
					return InchesPerWeeks;
				case SpeedType.InchesPerMonth:
					return InchesPerMonths;
				case SpeedType.InchesPerYear:
					return InchesPerYears;
				case SpeedType.InchesPerDecade:
					return InchesPerDecades;
				case SpeedType.InchesPerCentury:
					return InchesPerCenturies;
				case SpeedType.FeetPerNanosecond:
					return FeetPerNanoseconds;
				case SpeedType.FeetPerMicrosecond:
					return FeetPerMicroseconds;
				case SpeedType.FeetPerMillisecond:
					return FeetPerMilliseconds;
				case SpeedType.FeetPerSecond:
					return FeetPerSeconds;
				case SpeedType.FeetPerMinute:
					return FeetPerMinutes;
				case SpeedType.FeetPerHour:
					return FeetPerHours;
				case SpeedType.FeetPerDay:
					return FeetPerDays;
				case SpeedType.FeetPerWeek:
					return FeetPerWeeks;
				case SpeedType.FeetPerMonth:
					return FeetPerMonths;
				case SpeedType.FeetPerYear:
					return FeetPerYears;
				case SpeedType.FeetPerDecade:
					return FeetPerDecades;
				case SpeedType.FeetPerCentury:
					return FeetPerCenturies;
				case SpeedType.YardsPerNanosecond:
					return YardsPerNanoseconds;
				case SpeedType.YardsPerMicrosecond:
					return YardsPerMicroseconds;
				case SpeedType.YardsPerMillisecond:
					return YardsPerMilliseconds;
				case SpeedType.YardsPerSecond:
					return YardsPerSeconds;
				case SpeedType.YardsPerMinute:
					return YardsPerMinutes;
				case SpeedType.YardsPerHour:
					return YardsPerHours;
				case SpeedType.YardsPerDay:
					return YardsPerDays;
				case SpeedType.YardsPerWeek:
					return YardsPerWeeks;
				case SpeedType.YardsPerMonth:
					return YardsPerMonths;
				case SpeedType.YardsPerYear:
					return YardsPerYears;
				case SpeedType.YardsPerDecade:
					return YardsPerDecades;
				case SpeedType.YardsPerCentury:
					return YardsPerCenturies;
				case SpeedType.MilesPerNanosecond:
					return MilesPerNanoseconds;
				case SpeedType.MilesPerMicrosecond:
					return MilesPerMicroseconds;
				case SpeedType.MilesPerMillisecond:
					return MilesPerMilliseconds;
				case SpeedType.MilesPerSecond:
					return MilesPerSeconds;
				case SpeedType.MilesPerMinute:
					return MilesPerMinutes;
				case SpeedType.MilesPerHour:
					return MilesPerHours;
				case SpeedType.MilesPerDay:
					return MilesPerDays;
				case SpeedType.MilesPerWeek:
					return MilesPerWeeks;
				case SpeedType.MilesPerMonth:
					return MilesPerMonths;
				case SpeedType.MilesPerYear:
					return MilesPerYears;
				case SpeedType.MilesPerDecade:
					return MilesPerDecades;
				case SpeedType.MilesPerCentury:
					return MilesPerCenturies;
			}
			throw new Exception("Unknown SpeedType");
		}
	}
}