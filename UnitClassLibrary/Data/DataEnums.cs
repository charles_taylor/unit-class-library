using System;

 namespace UnitClassLibrary
{
	/// <summary> Enum for specifying the type of unit a Data is.</summary>
	public enum DataType { Bit, Byte, Kilobyte, Megabyte, Gigabyte, Terabyte, Petabyte, Exabyte, Zettabyte, Yottabyte };
}