using System;

 namespace UnitClassLibrary
{

	public partial class DataTransferRate
	{

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BitsPerNanoseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBitsPerNanoseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BitsPerNanosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BitsPerMicroseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBitsPerMicroseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BitsPerMicrosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BitsPerMilliseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBitsPerMilliseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BitsPerMillisecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BitsPerSeconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBitsPerSeconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BitsPerSecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BitsPerMinutes</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBitsPerMinutes(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BitsPerMinute, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BitsPerHours</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBitsPerHours(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BitsPerHour, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BitsPerDays</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBitsPerDays(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BitsPerDay, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BitsPerWeeks</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBitsPerWeeks(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BitsPerWeek, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BitsPerMonths</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBitsPerMonths(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BitsPerMonth, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BitsPerYears</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBitsPerYears(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BitsPerYear, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BitsPerDecades</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBitsPerDecades(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BitsPerDecade, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BitsPerCenturies</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBitsPerCenturies(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BitsPerCentury, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BytesPerNanoseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBytesPerNanoseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BytesPerNanosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BytesPerMicroseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBytesPerMicroseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BytesPerMicrosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BytesPerMilliseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBytesPerMilliseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BytesPerMillisecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BytesPerSeconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBytesPerSeconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BytesPerSecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BytesPerMinutes</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBytesPerMinutes(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BytesPerMinute, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BytesPerHours</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBytesPerHours(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BytesPerHour, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BytesPerDays</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBytesPerDays(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BytesPerDay, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BytesPerWeeks</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBytesPerWeeks(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BytesPerWeek, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BytesPerMonths</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBytesPerMonths(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BytesPerMonth, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BytesPerYears</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBytesPerYears(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BytesPerYear, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BytesPerDecades</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBytesPerDecades(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BytesPerDecade, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in BytesPerCenturies</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithBytesPerCenturies(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.BytesPerCentury, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in KilobytesPerNanoseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithKilobytesPerNanoseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.KilobytesPerNanosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in KilobytesPerMicroseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithKilobytesPerMicroseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.KilobytesPerMicrosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in KilobytesPerMilliseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithKilobytesPerMilliseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.KilobytesPerMillisecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in KilobytesPerSeconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithKilobytesPerSeconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.KilobytesPerSecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in KilobytesPerMinutes</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithKilobytesPerMinutes(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.KilobytesPerMinute, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in KilobytesPerHours</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithKilobytesPerHours(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.KilobytesPerHour, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in KilobytesPerDays</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithKilobytesPerDays(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.KilobytesPerDay, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in KilobytesPerWeeks</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithKilobytesPerWeeks(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.KilobytesPerWeek, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in KilobytesPerMonths</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithKilobytesPerMonths(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.KilobytesPerMonth, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in KilobytesPerYears</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithKilobytesPerYears(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.KilobytesPerYear, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in KilobytesPerDecades</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithKilobytesPerDecades(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.KilobytesPerDecade, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in KilobytesPerCenturies</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithKilobytesPerCenturies(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.KilobytesPerCentury, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in MegabytesPerNanoseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithMegabytesPerNanoseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.MegabytesPerNanosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in MegabytesPerMicroseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithMegabytesPerMicroseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.MegabytesPerMicrosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in MegabytesPerMilliseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithMegabytesPerMilliseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.MegabytesPerMillisecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in MegabytesPerSeconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithMegabytesPerSeconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.MegabytesPerSecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in MegabytesPerMinutes</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithMegabytesPerMinutes(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.MegabytesPerMinute, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in MegabytesPerHours</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithMegabytesPerHours(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.MegabytesPerHour, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in MegabytesPerDays</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithMegabytesPerDays(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.MegabytesPerDay, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in MegabytesPerWeeks</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithMegabytesPerWeeks(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.MegabytesPerWeek, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in MegabytesPerMonths</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithMegabytesPerMonths(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.MegabytesPerMonth, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in MegabytesPerYears</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithMegabytesPerYears(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.MegabytesPerYear, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in MegabytesPerDecades</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithMegabytesPerDecades(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.MegabytesPerDecade, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in MegabytesPerCenturies</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithMegabytesPerCenturies(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.MegabytesPerCentury, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in GigabytesPerNanoseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithGigabytesPerNanoseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.GigabytesPerNanosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in GigabytesPerMicroseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithGigabytesPerMicroseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.GigabytesPerMicrosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in GigabytesPerMilliseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithGigabytesPerMilliseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.GigabytesPerMillisecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in GigabytesPerSeconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithGigabytesPerSeconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.GigabytesPerSecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in GigabytesPerMinutes</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithGigabytesPerMinutes(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.GigabytesPerMinute, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in GigabytesPerHours</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithGigabytesPerHours(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.GigabytesPerHour, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in GigabytesPerDays</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithGigabytesPerDays(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.GigabytesPerDay, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in GigabytesPerWeeks</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithGigabytesPerWeeks(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.GigabytesPerWeek, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in GigabytesPerMonths</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithGigabytesPerMonths(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.GigabytesPerMonth, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in GigabytesPerYears</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithGigabytesPerYears(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.GigabytesPerYear, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in GigabytesPerDecades</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithGigabytesPerDecades(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.GigabytesPerDecade, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in GigabytesPerCenturies</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithGigabytesPerCenturies(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.GigabytesPerCentury, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in TerabytesPerNanoseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithTerabytesPerNanoseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.TerabytesPerNanosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in TerabytesPerMicroseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithTerabytesPerMicroseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.TerabytesPerMicrosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in TerabytesPerMilliseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithTerabytesPerMilliseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.TerabytesPerMillisecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in TerabytesPerSeconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithTerabytesPerSeconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.TerabytesPerSecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in TerabytesPerMinutes</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithTerabytesPerMinutes(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.TerabytesPerMinute, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in TerabytesPerHours</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithTerabytesPerHours(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.TerabytesPerHour, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in TerabytesPerDays</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithTerabytesPerDays(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.TerabytesPerDay, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in TerabytesPerWeeks</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithTerabytesPerWeeks(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.TerabytesPerWeek, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in TerabytesPerMonths</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithTerabytesPerMonths(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.TerabytesPerMonth, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in TerabytesPerYears</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithTerabytesPerYears(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.TerabytesPerYear, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in TerabytesPerDecades</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithTerabytesPerDecades(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.TerabytesPerDecade, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in TerabytesPerCenturies</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithTerabytesPerCenturies(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.TerabytesPerCentury, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in PetabytesPerNanoseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithPetabytesPerNanoseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.PetabytesPerNanosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in PetabytesPerMicroseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithPetabytesPerMicroseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.PetabytesPerMicrosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in PetabytesPerMilliseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithPetabytesPerMilliseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.PetabytesPerMillisecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in PetabytesPerSeconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithPetabytesPerSeconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.PetabytesPerSecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in PetabytesPerMinutes</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithPetabytesPerMinutes(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.PetabytesPerMinute, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in PetabytesPerHours</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithPetabytesPerHours(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.PetabytesPerHour, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in PetabytesPerDays</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithPetabytesPerDays(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.PetabytesPerDay, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in PetabytesPerWeeks</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithPetabytesPerWeeks(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.PetabytesPerWeek, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in PetabytesPerMonths</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithPetabytesPerMonths(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.PetabytesPerMonth, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in PetabytesPerYears</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithPetabytesPerYears(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.PetabytesPerYear, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in PetabytesPerDecades</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithPetabytesPerDecades(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.PetabytesPerDecade, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in PetabytesPerCenturies</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithPetabytesPerCenturies(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.PetabytesPerCentury, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ExabytesPerNanoseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithExabytesPerNanoseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ExabytesPerNanosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ExabytesPerMicroseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithExabytesPerMicroseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ExabytesPerMicrosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ExabytesPerMilliseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithExabytesPerMilliseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ExabytesPerMillisecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ExabytesPerSeconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithExabytesPerSeconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ExabytesPerSecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ExabytesPerMinutes</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithExabytesPerMinutes(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ExabytesPerMinute, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ExabytesPerHours</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithExabytesPerHours(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ExabytesPerHour, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ExabytesPerDays</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithExabytesPerDays(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ExabytesPerDay, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ExabytesPerWeeks</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithExabytesPerWeeks(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ExabytesPerWeek, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ExabytesPerMonths</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithExabytesPerMonths(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ExabytesPerMonth, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ExabytesPerYears</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithExabytesPerYears(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ExabytesPerYear, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ExabytesPerDecades</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithExabytesPerDecades(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ExabytesPerDecade, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ExabytesPerCenturies</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithExabytesPerCenturies(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ExabytesPerCentury, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ZettabytesPerNanoseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithZettabytesPerNanoseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ZettabytesPerNanosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ZettabytesPerMicroseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithZettabytesPerMicroseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ZettabytesPerMicrosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ZettabytesPerMilliseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithZettabytesPerMilliseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ZettabytesPerMillisecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ZettabytesPerSeconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithZettabytesPerSeconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ZettabytesPerSecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ZettabytesPerMinutes</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithZettabytesPerMinutes(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ZettabytesPerMinute, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ZettabytesPerHours</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithZettabytesPerHours(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ZettabytesPerHour, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ZettabytesPerDays</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithZettabytesPerDays(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ZettabytesPerDay, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ZettabytesPerWeeks</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithZettabytesPerWeeks(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ZettabytesPerWeek, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ZettabytesPerMonths</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithZettabytesPerMonths(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ZettabytesPerMonth, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ZettabytesPerYears</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithZettabytesPerYears(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ZettabytesPerYear, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ZettabytesPerDecades</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithZettabytesPerDecades(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ZettabytesPerDecade, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in ZettabytesPerCenturies</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithZettabytesPerCenturies(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.ZettabytesPerCentury, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in YottabytesPerNanoseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithYottabytesPerNanoseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.YottabytesPerNanosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in YottabytesPerMicroseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithYottabytesPerMicroseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.YottabytesPerMicrosecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in YottabytesPerMilliseconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithYottabytesPerMilliseconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.YottabytesPerMillisecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in YottabytesPerSeconds</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithYottabytesPerSeconds(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.YottabytesPerSecond, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in YottabytesPerMinutes</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithYottabytesPerMinutes(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.YottabytesPerMinute, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in YottabytesPerHours</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithYottabytesPerHours(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.YottabytesPerHour, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in YottabytesPerDays</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithYottabytesPerDays(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.YottabytesPerDay, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in YottabytesPerWeeks</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithYottabytesPerWeeks(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.YottabytesPerWeek, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in YottabytesPerMonths</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithYottabytesPerMonths(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.YottabytesPerMonth, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in YottabytesPerYears</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithYottabytesPerYears(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.YottabytesPerYear, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in YottabytesPerDecades</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithYottabytesPerDecades(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.YottabytesPerDecade, passedValue);
		}

		///<summary>Generator method that constructs DataTransferRate with assumption that the passed value is in YottabytesPerCenturies</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static DataTransferRate MakeDataTransferRateWithYottabytesPerCenturies(double passedValue)
		{
			return new DataTransferRate(DataTransferRateType.YottabytesPerCentury, passedValue);
		}
	}
}