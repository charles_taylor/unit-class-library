using System;

 namespace UnitClassLibrary
{

	public partial class DataTransferRate
	{

		public static DataTransferRate BitsPerNanosecond
		{
			get { return new DataTransferRate(DataTransferRateType.BitsPerNanosecond, 1); }
		}

		public static DataTransferRate BitsPerMicrosecond
		{
			get { return new DataTransferRate(DataTransferRateType.BitsPerMicrosecond, 1); }
		}

		public static DataTransferRate BitsPerMillisecond
		{
			get { return new DataTransferRate(DataTransferRateType.BitsPerMillisecond, 1); }
		}

		public static DataTransferRate BitsPerSecond
		{
			get { return new DataTransferRate(DataTransferRateType.BitsPerSecond, 1); }
		}

		public static DataTransferRate BitsPerMinute
		{
			get { return new DataTransferRate(DataTransferRateType.BitsPerMinute, 1); }
		}

		public static DataTransferRate BitsPerHour
		{
			get { return new DataTransferRate(DataTransferRateType.BitsPerHour, 1); }
		}

		public static DataTransferRate BitsPerDay
		{
			get { return new DataTransferRate(DataTransferRateType.BitsPerDay, 1); }
		}

		public static DataTransferRate BitsPerWeek
		{
			get { return new DataTransferRate(DataTransferRateType.BitsPerWeek, 1); }
		}

		public static DataTransferRate BitsPerMonth
		{
			get { return new DataTransferRate(DataTransferRateType.BitsPerMonth, 1); }
		}

		public static DataTransferRate BitsPerYear
		{
			get { return new DataTransferRate(DataTransferRateType.BitsPerYear, 1); }
		}

		public static DataTransferRate BitsPerDecade
		{
			get { return new DataTransferRate(DataTransferRateType.BitsPerDecade, 1); }
		}

		public static DataTransferRate BitsPerCentury
		{
			get { return new DataTransferRate(DataTransferRateType.BitsPerCentury, 1); }
		}

		public static DataTransferRate BytesPerNanosecond
		{
			get { return new DataTransferRate(DataTransferRateType.BytesPerNanosecond, 1); }
		}

		public static DataTransferRate BytesPerMicrosecond
		{
			get { return new DataTransferRate(DataTransferRateType.BytesPerMicrosecond, 1); }
		}

		public static DataTransferRate BytesPerMillisecond
		{
			get { return new DataTransferRate(DataTransferRateType.BytesPerMillisecond, 1); }
		}

		public static DataTransferRate BytesPerSecond
		{
			get { return new DataTransferRate(DataTransferRateType.BytesPerSecond, 1); }
		}

		public static DataTransferRate BytesPerMinute
		{
			get { return new DataTransferRate(DataTransferRateType.BytesPerMinute, 1); }
		}

		public static DataTransferRate BytesPerHour
		{
			get { return new DataTransferRate(DataTransferRateType.BytesPerHour, 1); }
		}

		public static DataTransferRate BytesPerDay
		{
			get { return new DataTransferRate(DataTransferRateType.BytesPerDay, 1); }
		}

		public static DataTransferRate BytesPerWeek
		{
			get { return new DataTransferRate(DataTransferRateType.BytesPerWeek, 1); }
		}

		public static DataTransferRate BytesPerMonth
		{
			get { return new DataTransferRate(DataTransferRateType.BytesPerMonth, 1); }
		}

		public static DataTransferRate BytesPerYear
		{
			get { return new DataTransferRate(DataTransferRateType.BytesPerYear, 1); }
		}

		public static DataTransferRate BytesPerDecade
		{
			get { return new DataTransferRate(DataTransferRateType.BytesPerDecade, 1); }
		}

		public static DataTransferRate BytesPerCentury
		{
			get { return new DataTransferRate(DataTransferRateType.BytesPerCentury, 1); }
		}

		public static DataTransferRate KilobytesPerNanosecond
		{
			get { return new DataTransferRate(DataTransferRateType.KilobytesPerNanosecond, 1); }
		}

		public static DataTransferRate KilobytesPerMicrosecond
		{
			get { return new DataTransferRate(DataTransferRateType.KilobytesPerMicrosecond, 1); }
		}

		public static DataTransferRate KilobytesPerMillisecond
		{
			get { return new DataTransferRate(DataTransferRateType.KilobytesPerMillisecond, 1); }
		}

		public static DataTransferRate KilobytesPerSecond
		{
			get { return new DataTransferRate(DataTransferRateType.KilobytesPerSecond, 1); }
		}

		public static DataTransferRate KilobytesPerMinute
		{
			get { return new DataTransferRate(DataTransferRateType.KilobytesPerMinute, 1); }
		}

		public static DataTransferRate KilobytesPerHour
		{
			get { return new DataTransferRate(DataTransferRateType.KilobytesPerHour, 1); }
		}

		public static DataTransferRate KilobytesPerDay
		{
			get { return new DataTransferRate(DataTransferRateType.KilobytesPerDay, 1); }
		}

		public static DataTransferRate KilobytesPerWeek
		{
			get { return new DataTransferRate(DataTransferRateType.KilobytesPerWeek, 1); }
		}

		public static DataTransferRate KilobytesPerMonth
		{
			get { return new DataTransferRate(DataTransferRateType.KilobytesPerMonth, 1); }
		}

		public static DataTransferRate KilobytesPerYear
		{
			get { return new DataTransferRate(DataTransferRateType.KilobytesPerYear, 1); }
		}

		public static DataTransferRate KilobytesPerDecade
		{
			get { return new DataTransferRate(DataTransferRateType.KilobytesPerDecade, 1); }
		}

		public static DataTransferRate KilobytesPerCentury
		{
			get { return new DataTransferRate(DataTransferRateType.KilobytesPerCentury, 1); }
		}

		public static DataTransferRate MegabytesPerNanosecond
		{
			get { return new DataTransferRate(DataTransferRateType.MegabytesPerNanosecond, 1); }
		}

		public static DataTransferRate MegabytesPerMicrosecond
		{
			get { return new DataTransferRate(DataTransferRateType.MegabytesPerMicrosecond, 1); }
		}

		public static DataTransferRate MegabytesPerMillisecond
		{
			get { return new DataTransferRate(DataTransferRateType.MegabytesPerMillisecond, 1); }
		}

		public static DataTransferRate MegabytesPerSecond
		{
			get { return new DataTransferRate(DataTransferRateType.MegabytesPerSecond, 1); }
		}

		public static DataTransferRate MegabytesPerMinute
		{
			get { return new DataTransferRate(DataTransferRateType.MegabytesPerMinute, 1); }
		}

		public static DataTransferRate MegabytesPerHour
		{
			get { return new DataTransferRate(DataTransferRateType.MegabytesPerHour, 1); }
		}

		public static DataTransferRate MegabytesPerDay
		{
			get { return new DataTransferRate(DataTransferRateType.MegabytesPerDay, 1); }
		}

		public static DataTransferRate MegabytesPerWeek
		{
			get { return new DataTransferRate(DataTransferRateType.MegabytesPerWeek, 1); }
		}

		public static DataTransferRate MegabytesPerMonth
		{
			get { return new DataTransferRate(DataTransferRateType.MegabytesPerMonth, 1); }
		}

		public static DataTransferRate MegabytesPerYear
		{
			get { return new DataTransferRate(DataTransferRateType.MegabytesPerYear, 1); }
		}

		public static DataTransferRate MegabytesPerDecade
		{
			get { return new DataTransferRate(DataTransferRateType.MegabytesPerDecade, 1); }
		}

		public static DataTransferRate MegabytesPerCentury
		{
			get { return new DataTransferRate(DataTransferRateType.MegabytesPerCentury, 1); }
		}

		public static DataTransferRate GigabytesPerNanosecond
		{
			get { return new DataTransferRate(DataTransferRateType.GigabytesPerNanosecond, 1); }
		}

		public static DataTransferRate GigabytesPerMicrosecond
		{
			get { return new DataTransferRate(DataTransferRateType.GigabytesPerMicrosecond, 1); }
		}

		public static DataTransferRate GigabytesPerMillisecond
		{
			get { return new DataTransferRate(DataTransferRateType.GigabytesPerMillisecond, 1); }
		}

		public static DataTransferRate GigabytesPerSecond
		{
			get { return new DataTransferRate(DataTransferRateType.GigabytesPerSecond, 1); }
		}

		public static DataTransferRate GigabytesPerMinute
		{
			get { return new DataTransferRate(DataTransferRateType.GigabytesPerMinute, 1); }
		}

		public static DataTransferRate GigabytesPerHour
		{
			get { return new DataTransferRate(DataTransferRateType.GigabytesPerHour, 1); }
		}

		public static DataTransferRate GigabytesPerDay
		{
			get { return new DataTransferRate(DataTransferRateType.GigabytesPerDay, 1); }
		}

		public static DataTransferRate GigabytesPerWeek
		{
			get { return new DataTransferRate(DataTransferRateType.GigabytesPerWeek, 1); }
		}

		public static DataTransferRate GigabytesPerMonth
		{
			get { return new DataTransferRate(DataTransferRateType.GigabytesPerMonth, 1); }
		}

		public static DataTransferRate GigabytesPerYear
		{
			get { return new DataTransferRate(DataTransferRateType.GigabytesPerYear, 1); }
		}

		public static DataTransferRate GigabytesPerDecade
		{
			get { return new DataTransferRate(DataTransferRateType.GigabytesPerDecade, 1); }
		}

		public static DataTransferRate GigabytesPerCentury
		{
			get { return new DataTransferRate(DataTransferRateType.GigabytesPerCentury, 1); }
		}

		public static DataTransferRate TerabytesPerNanosecond
		{
			get { return new DataTransferRate(DataTransferRateType.TerabytesPerNanosecond, 1); }
		}

		public static DataTransferRate TerabytesPerMicrosecond
		{
			get { return new DataTransferRate(DataTransferRateType.TerabytesPerMicrosecond, 1); }
		}

		public static DataTransferRate TerabytesPerMillisecond
		{
			get { return new DataTransferRate(DataTransferRateType.TerabytesPerMillisecond, 1); }
		}

		public static DataTransferRate TerabytesPerSecond
		{
			get { return new DataTransferRate(DataTransferRateType.TerabytesPerSecond, 1); }
		}

		public static DataTransferRate TerabytesPerMinute
		{
			get { return new DataTransferRate(DataTransferRateType.TerabytesPerMinute, 1); }
		}

		public static DataTransferRate TerabytesPerHour
		{
			get { return new DataTransferRate(DataTransferRateType.TerabytesPerHour, 1); }
		}

		public static DataTransferRate TerabytesPerDay
		{
			get { return new DataTransferRate(DataTransferRateType.TerabytesPerDay, 1); }
		}

		public static DataTransferRate TerabytesPerWeek
		{
			get { return new DataTransferRate(DataTransferRateType.TerabytesPerWeek, 1); }
		}

		public static DataTransferRate TerabytesPerMonth
		{
			get { return new DataTransferRate(DataTransferRateType.TerabytesPerMonth, 1); }
		}

		public static DataTransferRate TerabytesPerYear
		{
			get { return new DataTransferRate(DataTransferRateType.TerabytesPerYear, 1); }
		}

		public static DataTransferRate TerabytesPerDecade
		{
			get { return new DataTransferRate(DataTransferRateType.TerabytesPerDecade, 1); }
		}

		public static DataTransferRate TerabytesPerCentury
		{
			get { return new DataTransferRate(DataTransferRateType.TerabytesPerCentury, 1); }
		}

		public static DataTransferRate PetabytesPerNanosecond
		{
			get { return new DataTransferRate(DataTransferRateType.PetabytesPerNanosecond, 1); }
		}

		public static DataTransferRate PetabytesPerMicrosecond
		{
			get { return new DataTransferRate(DataTransferRateType.PetabytesPerMicrosecond, 1); }
		}

		public static DataTransferRate PetabytesPerMillisecond
		{
			get { return new DataTransferRate(DataTransferRateType.PetabytesPerMillisecond, 1); }
		}

		public static DataTransferRate PetabytesPerSecond
		{
			get { return new DataTransferRate(DataTransferRateType.PetabytesPerSecond, 1); }
		}

		public static DataTransferRate PetabytesPerMinute
		{
			get { return new DataTransferRate(DataTransferRateType.PetabytesPerMinute, 1); }
		}

		public static DataTransferRate PetabytesPerHour
		{
			get { return new DataTransferRate(DataTransferRateType.PetabytesPerHour, 1); }
		}

		public static DataTransferRate PetabytesPerDay
		{
			get { return new DataTransferRate(DataTransferRateType.PetabytesPerDay, 1); }
		}

		public static DataTransferRate PetabytesPerWeek
		{
			get { return new DataTransferRate(DataTransferRateType.PetabytesPerWeek, 1); }
		}

		public static DataTransferRate PetabytesPerMonth
		{
			get { return new DataTransferRate(DataTransferRateType.PetabytesPerMonth, 1); }
		}

		public static DataTransferRate PetabytesPerYear
		{
			get { return new DataTransferRate(DataTransferRateType.PetabytesPerYear, 1); }
		}

		public static DataTransferRate PetabytesPerDecade
		{
			get { return new DataTransferRate(DataTransferRateType.PetabytesPerDecade, 1); }
		}

		public static DataTransferRate PetabytesPerCentury
		{
			get { return new DataTransferRate(DataTransferRateType.PetabytesPerCentury, 1); }
		}

		public static DataTransferRate ExabytesPerNanosecond
		{
			get { return new DataTransferRate(DataTransferRateType.ExabytesPerNanosecond, 1); }
		}

		public static DataTransferRate ExabytesPerMicrosecond
		{
			get { return new DataTransferRate(DataTransferRateType.ExabytesPerMicrosecond, 1); }
		}

		public static DataTransferRate ExabytesPerMillisecond
		{
			get { return new DataTransferRate(DataTransferRateType.ExabytesPerMillisecond, 1); }
		}

		public static DataTransferRate ExabytesPerSecond
		{
			get { return new DataTransferRate(DataTransferRateType.ExabytesPerSecond, 1); }
		}

		public static DataTransferRate ExabytesPerMinute
		{
			get { return new DataTransferRate(DataTransferRateType.ExabytesPerMinute, 1); }
		}

		public static DataTransferRate ExabytesPerHour
		{
			get { return new DataTransferRate(DataTransferRateType.ExabytesPerHour, 1); }
		}

		public static DataTransferRate ExabytesPerDay
		{
			get { return new DataTransferRate(DataTransferRateType.ExabytesPerDay, 1); }
		}

		public static DataTransferRate ExabytesPerWeek
		{
			get { return new DataTransferRate(DataTransferRateType.ExabytesPerWeek, 1); }
		}

		public static DataTransferRate ExabytesPerMonth
		{
			get { return new DataTransferRate(DataTransferRateType.ExabytesPerMonth, 1); }
		}

		public static DataTransferRate ExabytesPerYear
		{
			get { return new DataTransferRate(DataTransferRateType.ExabytesPerYear, 1); }
		}

		public static DataTransferRate ExabytesPerDecade
		{
			get { return new DataTransferRate(DataTransferRateType.ExabytesPerDecade, 1); }
		}

		public static DataTransferRate ExabytesPerCentury
		{
			get { return new DataTransferRate(DataTransferRateType.ExabytesPerCentury, 1); }
		}

		public static DataTransferRate ZettabytesPerNanosecond
		{
			get { return new DataTransferRate(DataTransferRateType.ZettabytesPerNanosecond, 1); }
		}

		public static DataTransferRate ZettabytesPerMicrosecond
		{
			get { return new DataTransferRate(DataTransferRateType.ZettabytesPerMicrosecond, 1); }
		}

		public static DataTransferRate ZettabytesPerMillisecond
		{
			get { return new DataTransferRate(DataTransferRateType.ZettabytesPerMillisecond, 1); }
		}

		public static DataTransferRate ZettabytesPerSecond
		{
			get { return new DataTransferRate(DataTransferRateType.ZettabytesPerSecond, 1); }
		}

		public static DataTransferRate ZettabytesPerMinute
		{
			get { return new DataTransferRate(DataTransferRateType.ZettabytesPerMinute, 1); }
		}

		public static DataTransferRate ZettabytesPerHour
		{
			get { return new DataTransferRate(DataTransferRateType.ZettabytesPerHour, 1); }
		}

		public static DataTransferRate ZettabytesPerDay
		{
			get { return new DataTransferRate(DataTransferRateType.ZettabytesPerDay, 1); }
		}

		public static DataTransferRate ZettabytesPerWeek
		{
			get { return new DataTransferRate(DataTransferRateType.ZettabytesPerWeek, 1); }
		}

		public static DataTransferRate ZettabytesPerMonth
		{
			get { return new DataTransferRate(DataTransferRateType.ZettabytesPerMonth, 1); }
		}

		public static DataTransferRate ZettabytesPerYear
		{
			get { return new DataTransferRate(DataTransferRateType.ZettabytesPerYear, 1); }
		}

		public static DataTransferRate ZettabytesPerDecade
		{
			get { return new DataTransferRate(DataTransferRateType.ZettabytesPerDecade, 1); }
		}

		public static DataTransferRate ZettabytesPerCentury
		{
			get { return new DataTransferRate(DataTransferRateType.ZettabytesPerCentury, 1); }
		}

		public static DataTransferRate YottabytesPerNanosecond
		{
			get { return new DataTransferRate(DataTransferRateType.YottabytesPerNanosecond, 1); }
		}

		public static DataTransferRate YottabytesPerMicrosecond
		{
			get { return new DataTransferRate(DataTransferRateType.YottabytesPerMicrosecond, 1); }
		}

		public static DataTransferRate YottabytesPerMillisecond
		{
			get { return new DataTransferRate(DataTransferRateType.YottabytesPerMillisecond, 1); }
		}

		public static DataTransferRate YottabytesPerSecond
		{
			get { return new DataTransferRate(DataTransferRateType.YottabytesPerSecond, 1); }
		}

		public static DataTransferRate YottabytesPerMinute
		{
			get { return new DataTransferRate(DataTransferRateType.YottabytesPerMinute, 1); }
		}

		public static DataTransferRate YottabytesPerHour
		{
			get { return new DataTransferRate(DataTransferRateType.YottabytesPerHour, 1); }
		}

		public static DataTransferRate YottabytesPerDay
		{
			get { return new DataTransferRate(DataTransferRateType.YottabytesPerDay, 1); }
		}

		public static DataTransferRate YottabytesPerWeek
		{
			get { return new DataTransferRate(DataTransferRateType.YottabytesPerWeek, 1); }
		}

		public static DataTransferRate YottabytesPerMonth
		{
			get { return new DataTransferRate(DataTransferRateType.YottabytesPerMonth, 1); }
		}

		public static DataTransferRate YottabytesPerYear
		{
			get { return new DataTransferRate(DataTransferRateType.YottabytesPerYear, 1); }
		}

		public static DataTransferRate YottabytesPerDecade
		{
			get { return new DataTransferRate(DataTransferRateType.YottabytesPerDecade, 1); }
		}

		public static DataTransferRate YottabytesPerCentury
		{
			get { return new DataTransferRate(DataTransferRateType.YottabytesPerCentury, 1); }
		}
	}
}