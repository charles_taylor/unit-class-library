using System;

 namespace UnitClassLibrary
{
	/// <summary> Enum for specifying the type of unit a Stiffness is.</summary>
	public enum StiffnessType { NewtonsPerMillimeter, NewtonsPerCentimeter, NewtonsPerMeter, NewtonsPerKilometer, NewtonsPerInch, NewtonsPerFoot, NewtonsPerYard, NewtonsPerMile, PoundsPerMillimeter, PoundsPerCentimeter, PoundsPerMeter, PoundsPerKilometer, PoundsPerInch, PoundsPerFoot, PoundsPerYard, PoundsPerMile, KipsPerMillimeter, KipsPerCentimeter, KipsPerMeter, KipsPerKilometer, KipsPerInch, KipsPerFoot, KipsPerYard, KipsPerMile }
}